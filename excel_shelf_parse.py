import xlrd
import json

def get_height_price(height):
    return 300*((height-210)/10)


def parse_shelf(filename):
    book = xlrd.open_workbook(filename, formatting_info=True)
    sheet = book.sheet_by_index(0)
    items = []
    type = 1
    price = 0
    for nrow in range(0, sheet.nrows-1):
        row = sheet.row_values(nrow)
        # if int(row[16]) > 1:
        #     item = [1, row[16], row[17], row[18], row[19], row[20], row[21], row[22], row[23], row[24]]
        #     items.append(item)
        #     print(item)
        # else:
        #     nrow += 1
        try:
            if int(row[16]) != 450:
                korpus_type = "korpus"
                if type == 3:
                    korpus_type += "2"

                elif type == 4:
                    korpus_type += "3"
                elif type == 5:
                    korpus_type += "4"
                elif type == 7:
                    korpus_type += "5"
                item = [type, price, row[16], row[17], row[18], row[19], row[20], row[21], row[22], row[23], row[24], korpus_type]
                items.append(recalc_price(item))
        except:
            try:
                price = int(row[4])
            except:
                type += 1
    korpuss = ["korpus2", "korpus3", "korpus4", "korpus5"]
    for korp in korpuss:
        main_json_sections = []
        for k in range(0, 8):
            types = "type27_" + str(45 + 5 * k)
            low_jsons = []
            for z in range(0, 6):
                if korp == korpuss[0]:
                    width = 100
                elif korp == korpuss[1]:
                    width = 170
                elif korp == korpuss[2]:
                    width = 260
                elif korp == korpuss[3]:
                    width = 320
                for item in items:
                    if item[8] == korp:
                        low_json = {
                            "W": width,
                            "H": 220 + 10*z,
                            "price": item[k]+get_height_price(220+10*z)
                        }
                        low_jsons.append(low_json)
                        width += 10
                        print width
            jsonss = {
                "variants": low_jsons,
                "type": types
            }
            main_json_sections.append(jsonss)
        main_json = {
            "type": korp,
            "sections": main_json_sections
        }
        out = open(korp+".json", 'w+')
        out.write(json.dumps(main_json))
        out.close()



def recalc_price(item):
    #todo save
    out = [item[0], item[2]*item[1], item[3]*item[1], item[4]*item[1], item[5]*item[1], item[6]*item[1],
           item[7]*item[1], item[8]*item[1], item[9]*item[1], item[11]]
    out = [int(round(out[1])), int(round(out[2])), int(round(out[3])), int(round(out[4])), int(round(out[5])),
           int(round(out[6])), int(round(out[7])), int(round(out[8])), out[9]]
    print out
    return out


if __name__ == '__main__':
    parse_shelf("1111.xls")
